package com.maven.step_def;

import com.maven.pages.RegistrationPage;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.Random;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.hamcrest.core.Is.is;

public class RegistrationPageStep {
    RegistrationPage registrationPage = new RegistrationPage();
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(1000);

    @When("^I enter my first name \"([^\"]*)\"$")
    public void i_enter_my_first_name(String firstName) throws Throwable {
        registrationPage.enterFirstName(firstName);
    }

    @When("^I enter my last name \"([^\"]*)\"$")
    public void i_enter_my_last_name(String lastName) throws Throwable {
        registrationPage.enterLastName(lastName);
    }

    @When("^I enter my email address \"([^\"]*)\"$")
    public void i_enter_my_email_address(String email) throws Throwable {
        registrationPage.enterEmail(randomInt + email);
    }

    @When("^I retype my email address \"([^\"]*)\"$")
    public void i_retype_my_email_address(String email) throws Throwable {
        registrationPage.reTypeEmail(randomInt + email);
    }

    @When("^I enter existing email address \"([^\"]*)\"$")
    public void i_enter_existing_email_address(String email) throws Throwable {
        registrationPage.enterEmail(email);
    }

    @When("^I retype existing email address \"([^\"]*)\"$")
    public void i_retype_existing_email_address(String email) throws Throwable {
        registrationPage.reTypeEmail(email);
    }

    @When("^I enter my password \"([^\"]*)\"$")
    public void i_enter_my_password(String password) throws Throwable {
        registrationPage.enterPassword(password);
    }

    @When("^I retype my password \"([^\"]*)\"$")
    public void i_retype_my_password(String password) throws Throwable {
        registrationPage.reTypePassword(password);
    }

    @When("^I enter my phone number \"([^\"]*)\"$")
    public void i_enter_my_phone_number(String phoneNumber) throws Throwable {
        registrationPage.enterPhoneNumber(phoneNumber);
    }

    @When("^I click on register now button$")
    public void i_click_on_register_now_button() throws Throwable {
        registrationPage.clickRegisterNowButton();
    }

    @Then("^I should see form model with text \"([^\"]*)\"$")
    public void i_should_see_form_model_with_text(String message) throws Throwable {
        String actualText = registrationPage.getRegistrationMsg();
        assertThat(actualText, is(equalToIgnoringCase(message)));
    }

    @Then("^User should see error message \"([^\"]*)\"$")
    public void user_should_see_error_message(String errorMsg) throws Throwable {
        String actualErrorMsg = registrationPage.getErrorMsg();
        assertThat(actualErrorMsg, is(equalToIgnoringCase(errorMsg)));
    }

    @Then("^User should see warning message \"([^\"]*)\"$")
    public void user_should_see_warning_message(String warnigMsg) throws Throwable {
        assertThat(registrationPage.getWarningMsg(), is(equalToIgnoringCase(warnigMsg)));
    }


}
