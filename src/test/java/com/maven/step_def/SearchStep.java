package com.maven.step_def;

import com.maven.pages.HomePage;
import com.maven.pages.SearchResultPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class SearchStep {
    HomePage homePage = new HomePage();
    SearchResultPage searchResultPage = new SearchResultPage();

    @Given("^I am on the home page$")
    public void i_am_on_home_page() throws Throwable {
        String actual = homePage.getCurrentUrl();
        assertThat(actual, is(endsWith("takealot.com/")));
    }

    @When("^I click on the register button$")
    public void i_click_on_see_your_rates_button() throws Throwable {
        homePage.clickRegisterButton();
    }

    @When("^I search for products \"([^\"]*)\"$")
    public void i_search_for(String item) throws Throwable {
        homePage.searchProducts(item);
    }

    @Then("^I should be able to see respective results$")
    public void i_should_be_able_to_see_respective_results() {
        String actual = searchResultPage.getProductHeader();
        assertThat(actual, containsString(homePage.searchItem));
    }
}
