package com.maven.step_def;

import com.maven.pages.BasketPage;
import com.maven.pages.ProductDescriptionPage;
import com.maven.pages.SearchResultPage;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class BasketStep {
    SearchResultPage searchResultPage = new SearchResultPage();
    ProductDescriptionPage productDescriptionPage = new ProductDescriptionPage();
    BasketPage basketPage = new BasketPage();


    @When("^I click load more button$")
    public void i_click_load_more_button() throws Throwable {
        searchResultPage.clickOnLoadMoreButton();
    }

    @When("^I select a product \"([^\"]*)\"$")
    public void i_select_a_product(String productName) throws Throwable {
        searchResultPage.selectProduct(productName);
    }

    @When("^I add the product to the basket$")
    public void i_add_the_product_to_the_basket() throws Throwable {
        productDescriptionPage.addToCart();
        productDescriptionPage.goToCart();
    }

    @Then("^the product should be in the basket$")
    public void i_should_see_both_products_in_the_basket() throws Throwable {
        List<String> productList = basketPage.getProductsInBaskets();
        System.out.println(productList);
        for (String item : productList) {
            assertThat(item, containsString(SearchResultPage.selectedProduct));
        }

    }
}
