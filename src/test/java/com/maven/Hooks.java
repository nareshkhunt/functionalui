package com.maven;

import com.maven.driver.DriverManager;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {
    private DriverManager driverManager = new DriverManager();

    @Before
    public void setUp() {
        driverManager.openBrowser();
        driverManager.navigateTo("https://www.takealot.com/");
        driverManager.maxBroser();
        driverManager.applyImplicit();
    }

    @After
    public void tearDown() {
        driverManager.closeBrowser();
    }
}
