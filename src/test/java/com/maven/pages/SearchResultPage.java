package com.maven.pages;

import com.maven.driver.DriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SearchResultPage extends DriverManager {

    Actions actions = new Actions(driver);
    public static String selectedProduct;

    @FindBy(css = ".search-count.toolbar-module_search-count_P0ViI")
    private WebElement headerText;

    @FindBy(xpath = "//h4[@class='product-title']/span")
    private List<WebElement> productNames;

    @FindBy(xpath = "//button[contains(text(),'Load More')]")
    private WebElement loadMoreButton;

    public String getProductHeader() {
        return headerText.getText();
    }

    public void clickOnLoadMoreButton() {
        loadMoreButton.click();
        sleep(5000);
    }

    public void selectProduct(String productName) {
        selectedProduct = productName;
        for (WebElement productElement : productNames) {
            if (productElement.getText().contains(productName)) {
                actions.moveToElement(productElement).click().perform();
                break;
            }
        }
    }
}
