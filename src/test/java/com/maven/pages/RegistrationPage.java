package com.maven.pages;

import com.maven.driver.DriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegistrationPage extends DriverManager {
    @FindBy(id = "firstname")
    private WebElement firstNameText;

    @FindBy(id = "surname")
    private WebElement lastNametext;

    @FindBy(id = "email")
    private WebElement emailField;

    @FindBy(id = "email2")
    private WebElement reTypeEmailField;

    @FindBy(id = "password")
    private WebElement passwordField;

    @FindBy(id = "password2")
    private WebElement reTypePassword;

    @FindBy(id = "telno1")
    private WebElement phoneNumberBox;

    @FindBy(css = "input[type=submit]")
    private WebElement registerNowButton;

    @FindBy(xpath = "//h3[contains(text(),'Welcome to the TAKEALOT.com family!')]")
    private WebElement successfulMsgText;

    @FindBy(id = "invalid-email")
    private WebElement errorMsg;

    @FindBy(xpath = "//div[contains(text(),'That email address is already registered on takeal')]")
    private WebElement warningMsg;

    public void enterFirstName(String firstName) {
        firstNameText.sendKeys(firstName);
    }

    public void enterLastName(String lastName) {
        lastNametext.sendKeys(lastName);
    }

    public void enterEmail(String email) {
        emailField.sendKeys(email);
    }

    public void reTypeEmail(String email) {
        reTypeEmailField.sendKeys(email);
    }

    public void enterPassword(String password) {
        passwordField.sendKeys(password);
    }

    public void reTypePassword(String password) {
        reTypePassword.sendKeys(password);
    }

    public void enterPhoneNumber(String phoneNumber) {
        phoneNumberBox.sendKeys(phoneNumber);
    }

    public void clickRegisterNowButton() {
        registerNowButton.click();
    }

    public String getRegistrationMsg() {
        return successfulMsgText.getText();
    }

    public String getErrorMsg() {
        return errorMsg.getText();
    }

    public String getWarningMsg() {
        return warningMsg.getText();
    }

    public String getFormPageUrl() {

        return driver.getCurrentUrl();
    }
}
