package com.maven.pages;

import com.maven.driver.DriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends DriverManager {

    public static String searchItem;

    @FindBy(xpath = "//a[contains(text(),'Register')]")
    private WebElement registerButton;

    @FindBy(css = "input[name=search]")
    private WebElement searchBox;

    @FindBy(css = "button.search-btn.search-icon")
    private WebElement magnifierGlass;

    public void clickRegisterButton() {
        waitUntilElementClickable(registerButton).click();
    }

    public String getCurrentUrl() {
        return driver.getCurrentUrl();
    }

    public void searchProducts(String item) {
        searchItem = item;
        searchBox.sendKeys(item);
        magnifierGlass.click();
    }
}
