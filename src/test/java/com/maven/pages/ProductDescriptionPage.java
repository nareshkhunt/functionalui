package com.maven.pages;

import com.maven.driver.DriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProductDescriptionPage extends DriverManager {

    @FindBy(xpath = "//div[@class='buybox-actions']/div/div[2]/button")
    private WebElement addToCartButton;

    @FindBy(xpath = "//div[@class='buybox-actions']/div/div[3]/div/button")
    private WebElement goToCartButton;

    public void addToCart() {
        waitUntilElementClickable(addToCartButton).click();
    }

    public void goToCart() {
        waitUntilElementClickable(goToCartButton).click();
    }
}
